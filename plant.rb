module Plant
	extend self
	def health_level
		[:dead, :sad, :ok, :happy]
	end
	def health_display
		['];', '):', '|:', '(:']
	end
	def stage_data
		[
			{stage: :seed, drops: (2..5), sun: (2..4), food: (0..2), growth: ['o', '', '', '']},
			{stage: :sprout, drops: (4..7), sun: (5..7), food: (1..3), growth: ['', '>', '-', '--']},
			{stage: :young, drops: (6..8), sun: (7..9), food: (2..6), growth: ['', '-', '-', '--']},
			{stage: :adult, drops: (8..10), sun: (8..10), food: (5..8), growth: ['', '-', '-', '--']},
			{stage: :mature, drops: (6..8), sun: (7..9), food: (4..6), growth: ['', '-', '-', '-']},
			{stage: :harvest, drops: (2..4), sun: (5..7), food: (1..3), growth: ['', '-', '-', '-']}
		]
	end
	def plant_data
		{
			day: 1,
			sun: 0,
			drops: 0,
			food: 0,
			growth: [],
			history: []
		}
	end
	def give_sun plant_data, hours
		plant_data[:sun] += hours
		plant_data
	end
	def give_water plant_data, drops
		plant_data[:drops] += drops
		plant_data
	end
	def give_food plant_data, pills
		plant_data[:food] += pills
		plant_data
	end
	def new_day yesterday
		today = plant_data()
		if yesterday
			today[:day] = yesterday[:day] + 1
			today[:history] = yesterday[:history] + yesterday[:growth]
		end
		today
	end
	def prompt_new_day?
		while true
			print "Start a new day? (y/n) "
			input = gets.strip.downcase
			return true if input == 'y' or input == 'yes' or input == "1"
			return false if input == 'n' or input == 'no' or input == "0"
			puts "try again..."
		end
	end
	def prompt_sun?
		while true
			input = nil
			begin	
				print " Give your plant some sun! Enter hours:? (0 to 10) "
				input = gets.strip.to_i
			rescue
				input = nil
			end
			return input if input and input >= 0 and input <= 10
			puts "try again..."
		end
	end
	def prompt_water?
		while true
			input = nil
			begin
				print " Give your plant some water! Enter drops:? (0 to 10) "
				input = gets.strip.to_i
			rescue
				input = nil
			end
			return input if input and input >= 0 and input <= 10
			puts "try again..."
		end
	end
	def prompt_food?
		while true
			input = nil
			begin
				print " Give your plant some food! Enter pills:? (0 to 10) "
				input = gets.strip.to_i
			rescue
				input = nil
			end
			return input if input and input >= 0 and input <= 10
			puts "try again..."
		end
	end
	def get_stage today
		stage = stage_data[today[:day] - 1]
	end
	def get_health_index today
		stage = get_stage(today)

		health_index = 0
		health_index += 1 if stage[:drops] === today[:drops]
		health_index += 1 if stage[:sun] === today[:sun]
		health_index += 1 if stage[:food] === today[:food]

		health_index
	end
	def get_health today
		health_level[get_health_index(today)]
	end
	def get_harvest today
		(today[:growth] + today[:history]).join('').length
	end
	def grow today
		stage = get_stage today
		health_index = get_health_index(today)
		today[:growth] = stage[:growth][0..health_index]
		today
	end
	def show_new_day today
		puts " Day #{today[:day]} -- #{get_stage(today)[:stage]}"
		puts ""
	end
	def show_growth today
		health_index = get_health_index(today)
		#puts today[:history].inspect
		#puts today[:growth].inspect
		plant_graphic = today[:history].join('') + today[:growth].join('') + health_display[health_index]

		#puts "growing!!!... #{today.inspect}"
		puts ""
		puts "    growing... your plant is: #{get_health(today)}"
		puts ""
		puts "    #{plant_graphic}"
		puts ""
	end
	def main
		today = nil
		puts ""
		puts " ----------------------------------------------------------------"
		puts " Welcome to Plant Grow!"
		puts " Each day will be the next stage of your plant's growth."
		puts " Give your plant water drops, sunlight hours, and food pills."
		puts " Don't give it too much or too little :("
		puts " ----------------------------------------------------------------"
		puts ""
		while true
			today = new_day(today) #if today == nil or prompt_new_day?
			if today
				show_new_day today
				today = give_water(today, prompt_water?)
				today = give_sun(today, prompt_sun?)
				today = give_food(today, prompt_food?)
				today = grow(today)
				show_growth today
				if get_stage(today)[:stage] == :harvest
					puts "  Your harvest was #{get_harvest(today)}"
					puts ""
					break
				end
				if get_health(today) == :dead
					puts "  You killed your plant! :("
					puts ""
					break
				end
			end
		end
		#todo: track wasted water, sun, food... and give feedback...?
		#todo: adjust harvest based on optimal water, sun, food, etc.
	end
end

Plant.main

# >----o:| ok plant     :|
# >----8:) happy plant! :)
# >----x:( sad plant    :(
# >----\:\ plant died

# o(:
# o|:
# o):
# o>----(: happy
# o>----|: ok
# o>----): sad
# o>----]; dead

